import { Test, TestingModule } from "@nestjs/testing";
import { HttpServer, HttpStatus, INestApplication } from "@nestjs/common";
import * as request from "supertest";
import { CoffeesModule } from "../../src/coffees/coffees.module";
import { TypeOrmModule } from "@nestjs/typeorm";
import { CreateCoffeeDto } from "../../src/coffees/dto/create-coffee.dto";
import { UpdateCoffeeDto } from "../../src/coffees/dto/update-coffee.dto";

describe("AppController (e2e)", () => {
  let app: INestApplication;
  let httpServer: HttpServer;

  const coffee = {
    name: "Ship roast",
    brand: "Brew",
    flavors: ["choc", "van"],
  };

  const coffeeTwo = {
    name: "coffeeTwo",
    brand: "NoBrew",
    flavors: ["van"],
  };

  const expectedPartialCoffee = expect.objectContaining({
    ...coffee,
    flavors: expect.arrayContaining(
      coffee.flavors.map((name) => expect.objectContaining({ name }))
    ),
  });

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [
        CoffeesModule,
        TypeOrmModule.forRoot({
          type: "postgres",
          host: "localhost",
          port: 5433,
          username: "postgres",
          password: "pass123",
          database: "postgres",
          autoLoadEntities: true,
          synchronize: true,
        }),
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
    httpServer = app.getHttpServer();
  });

  it("/ Create Post", () => {
    return request(httpServer)
      .post("/coffees")
      .send(coffee as CreateCoffeeDto)
      .expect(HttpStatus.CREATED)
      .then(({ body }) => {
        expect(body).toEqual(expectedPartialCoffee);
        return request(httpServer)
          .post("/coffees")
          .send(coffeeTwo as CreateCoffeeDto)
          .expect(HttpStatus.CREATED);
      });
  });

  it.todo("GET all");
  it.todo("Get one");
  it.todo("Update one");
  it.todo("Delete on");

  afterAll(async () => {
    await app.close();
  });
});
